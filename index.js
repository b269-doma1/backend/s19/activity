/*
    1. Declare 3 variables without initialization called username,password and role.
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/

let username;
let password;
let role;



function login(){
    username = prompt ("Enter username");
    password = prompt ("Enter password");
    role = prompt ("Enter role");

    if(username === "" || username === null || password === null || role === "" || role === null){
        alert('Input must NOT be empty');
    } 
    else {
        switch(role){
            case "admin":
                 alert("Welcome back to the class portal, admin");
                 break;
            case "teacher":
                alert("Welcome back to the class portal, teacher");
                break;
            case "student":
                 alert("Welcome back to the class portal, student");
                break;
            default:
                alert("Role doesn't exist");
                break;    

        }
    }
  

}

login();

function checkAverage(first_grade, second_grade, third_grade, fourth_grade){

    let average = (first_grade + second_grade + third_grade + fourth_grade) / 4;

    if(role === "admin" || role === "teacher" || role === undefined || role === null){
            alert("You are not allowed to access this feature!");
        } else {
            if(average <= 74) {
                console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
            } else if (average >= 75 && average <= 79) {
                console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
            } else if (average >= 80 && average <= 84) {
                console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
            } else if (average >= 85 && average <= 89) {
                console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
            } else if (average >= 90 && average <= 95) {
                console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
            } else if (average >= 96) {
                console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
            }
        }
    }







    



